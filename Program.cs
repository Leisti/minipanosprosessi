﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading;
using System.Threading.Tasks;
using Tuni.MppOpcUaClientLib;

namespace minipanosprosessi
{
    class Program : IDisposable
    {
        private static ConnectionParamsHolder opcParams = new ConnectionParamsHolder("TO DO");
        private static MppClient opcClient = new MppClient(opcParams);
        private static Dictionary<string, MppValue> changedItemsDict = new Dictionary<string, MppValue>();

        private object m_lockObject = null;
        private bool m_running = false;
        private bool m_stopSequence = false;

        // parameter initialization
        private static int T100WaterLevel = 216;
        private static int T200WaterLevel = 90;
        private static int T400WaterLevel = 90;
        private static double T300Temperature = 20;
        private static int T300Pressure = 0;
        private static bool T100InSafetyLevel = false;

        private HashSet<IBackgroundTaskObserver> m_observers = null;

        private static int saturationTime = 0; // [ms]
        private static int cookingTime = 0; // [ms]

        private static double cookingTemperature = 0; // [C]
        private static int cookingPressure = 0; // [hPa]

        // constant values
        private const int LI100_SAFETY_LEVEL = 100; // [mm]
        private const double PRESSURE_DIFF_TO_CONTROL = 5; // [hPa]
        private const double TEMP_DIFF_TO_CONTROL = 0.1; // [C]
        private const int MAX_PRESSURE_DIFF = 10; // [hPa]
        private const double MAX_TEMP_DIFF = 0.3; // [C]
        private const int DELAY_TIME = 1000; // [ms]

        /// <summary>
        /// Kutsuu opcClientin rakentajaa sekä lisää sille tapahtumakäsittelijät. Lisää kaikki tarvittavat
        /// prosessilaitteet "tilaukseen".
        /// </summary>
        public Program()
        {
            m_lockObject = new object();

            m_observers = new HashSet<IBackgroundTaskObserver>();

            //Connection event
            opcClient.ConnectionStatus += new MppClient.ConnectionStatusEventHandler(opcConnectionEvent);
            opcClient.ProcessItemsChanged += new MppClient.ProcessItemsChangedEventHandler(opcItemChange);

            try
            {
                opcClient.Init();
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine("Caught: InvalidOperationException | when calling Init()");
            }

            lock (m_lockObject)
            {
                opcClient.AddToSubscription("LI100");
                opcClient.AddToSubscription("LI200");
                opcClient.AddToSubscription("LI400");
                opcClient.AddToSubscription("PI300");
                opcClient.AddToSubscription("TI300");
                opcClient.AddToSubscription("LS+300");
                opcClient.AddToSubscription("LS-300");
                opcClient.AddToSubscription("V201");
                opcClient.AddToSubscription("LA+100");
            }
            
        }

        /// <summary>
        /// Suorittaa järjestyksessä kunkin sekvenssin vaiheen, mikäli aiempi vaihe on onnistuneesti suoritettu.
        /// Jos aiempaa vaihetta ei ole onnistuneesti suoritettu, aiheutuu virheilmoitus ja sekvenssi keskeytetään.
        /// </summary>
        public void ExecuteSequence()
        {
            lock (m_lockObject)
            {
                if (m_running) return;

                m_running = true;
                m_stopSequence = false;
            }

            ObserverShowMessage("Impregnation in process");
            bool impregnationSuccess = Impregnation();
            if (!impregnationSuccess)
            {
                ObserverShowMessage("Impregnation failed or sequence stopped");
                ResetEquipment();
                return;
            }

            ObserverShowMessage("Black Liquor Fill in process");
            bool blackLiqFillSuccess = BlackLiquorFill();
            if (!blackLiqFillSuccess)
            {
                ObserverShowMessage("Black Liquor Fill failed or sequence stopped");
                ResetEquipment();
                return;
            }

            ObserverShowMessage("White Liquor Fill in process");
            bool whiteLiqFillSuccess = WhiteLiquorFill();
            if (!whiteLiqFillSuccess)
            {
                ObserverShowMessage("White Liquor Fill failed or sequence stopped");
                ResetEquipment();
                return;
            }

            ObserverShowMessage("Cooking in process");
            bool cookingSuccess = Cooking();
            if (!cookingSuccess)
            {
                ObserverShowMessage("Cooking failed or sequence stopped");
                ResetEquipment();
                return;
            }

            ObserverShowMessage("Discharge in process");
            bool dischargeSuccess = Discharge();
            if (!dischargeSuccess)
            {
                ObserverShowMessage("Discharge failed or sequence stopped");
                ResetEquipment();
                return;
            }

            ObserverShowMessage("Sequence finished succesfully");
            ResetEquipment();
        }
        
        /// <summary>
        /// Muuttaa totuusarvon true muuttujalle m_stopSequence, jos sekvenssi on käynnissä
        /// </summary>
        public void StopSequence()
        {
            lock (m_lockObject)
            {
                if (!m_running) return;

                m_stopSequence = true;
            }
        }

        /// <summary>
        /// Asettaa pumput ja venttiilit alkutilaan
        /// </summary>
        private void ResetEquipment()
        {
            List<string> controlValves = new List<string>() { "V102", "V104" };
            List<string> controlPumps = new List<string>() { "P100", "P200" };

            List<string> onOffItems = new List<string>(){"V401","V204","V302","V404","V303",
                                                         "V103","V301","V304","V201",
                                                         "E100","P100_P200_PRESET"};
            for (int i = 0; i < controlValves.Count; i++)
            {
                opcClient.SetValveOpening(controlValves[i], 0);
            }
            for (int i = 0; i < controlPumps.Count; i++)
            {
                opcClient.SetPumpControl(controlPumps[i], 0);
            }
            for (int i = 0; i < onOffItems.Count; i++)
            {
                opcClient.SetOnOffItem(onOffItems[i], false);
            }

            lock (m_lockObject)
            {
                m_running = false;
            }
        }

        /// <summary>
        /// Päivittää luokkaan käyttäjän antamat parametrit, mikäli ne ovat kelvolliset ja sekvenssi ei ole käynnissä
        /// </summary>
        /// <param name="newSatTime">saturation time</param>
        /// <param name="newCookTime">cooking time</param>
        /// <param name="newTemp">temperature</param>
        /// <param name="newPressure">pressure</param>
        public void ChangeInitValues(int newSatTime, int newCookTime, double newTemp, int newPressure)
        {
            if (m_running)
            {
                string valueString = (saturationTime / 1000).ToString() + " s, " + (cookingTime / 1000).ToString() + " s, "
                    + cookingTemperature.ToString() + " C, " + cookingPressure.ToString() + " hPa.";
                ObserverShowMessage("Prosessi on jo käynnissä, joten parametreja ei voi muuttaa. Käytössä olevat parametrit ovat: " + valueString);
            }
            else if (newSatTime < 0 || newCookTime < 0 || newTemp < 20 || newPressure < 0)
            {
                ObserverShowMessage("Annetut parametrit eivät ole kelvolliset");
            }
            else 
            {
                saturationTime = newSatTime * 1000;
                cookingTime = newCookTime * 1000;
                cookingTemperature = newTemp;
                cookingPressure = newPressure;

                string valueString = (saturationTime / 1000).ToString() + " s, " + (cookingTime / 1000).ToString() + " s, "
                    + cookingTemperature.ToString() + " C, " + cookingPressure.ToString() + " hPa.";
                ObserverShowMessage("Parametrit vaihdettu. Uudet parametrit ovat: " + valueString);
            } 
        }

        /// <summary>
        /// Tapahtumankäsittelijä yhteyden tilan muutoksille. Tulostaa nykyisen yhteyden tilan: connected/disconnected.
        /// </summary>
        /// <param name="source">source object</param>
        /// <param name="args">Event Arguments of changed Connection Status</param>
        public void opcConnectionEvent(object source, ConnectionStatusEventArgs args)
        {
            if (args.StatusInfo.FullStatusString == "Disconnected")
            { ObserverShowMessage("Lost connection"); }
        }

        /// <summary>
        /// Tapahtumankäsittelijä prosessilaitteiden mittausarvojen ja tilojen muutoksille.
        /// </summary>
        /// <param name="source">source object</param>
        /// <param name="args">Event Arguments of changed process items</param>
        private void opcItemChange(object source, ProcessItemChangedEventArgs args)
        {
            changedItemsDict = args.ChangedItems;

            lock (m_lockObject)
            {
                try
                {
                    T100WaterLevel = (int)changedItemsDict["LI100"].GetValue();
                }
                catch (KeyNotFoundException e) { }
                try
                {
                    T200WaterLevel = (int)changedItemsDict["LI200"].GetValue();
                }
                catch (KeyNotFoundException e) { }
                try
                {
                    T400WaterLevel = (int)changedItemsDict["LI400"].GetValue();
                }
                catch (KeyNotFoundException e) { }
                try
                {
                    T300Temperature = (double)changedItemsDict["TI300"].GetValue();
                }
                catch (KeyNotFoundException e) { }
                try
                {
                    T300Pressure = (int)changedItemsDict["PI300"].GetValue();
                }
                catch (KeyNotFoundException e) { }
                try
                {
                    T100InSafetyLevel = (bool)changedItemsDict["LA+100"].GetValue();
                }
                catch (KeyNotFoundException e) { }
            }     

            NotifyObserversOfValueChange();
        }

        /// <summary>
        /// Suorittaa sekvenssin aliprosessiin Impregnation kuuluvat vaiheet.
        /// </summary>
        /// <returns>totuusarvo: "true" jos Impregnation suoritettiin onnistuneesti loppuun asti, muuten "false"</returns>
        private bool Impregnation()
        {
            try
            {
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM2_OP1();
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM5_OP1();
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM3_OP2();
                
                bool move_on = false;
                while (move_on == false)
                {
                    lock (m_lockObject) { if (m_stopSequence)return false; }
                    try { move_on = (bool)changedItemsDict["LS+300"].GetValue(); }
                    catch (KeyNotFoundException e) { }
                }
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM3_OP1();

                System.Threading.Thread.Sleep(saturationTime);

                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM2_OP2();
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM5_OP3();
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM3_OP6();
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM3_OP8();

                return true;
            }
            catch (Exception ex) // erittele toimenpiteet eri poikkeuksille
            {
                Console.WriteLine("Caught Impregnation()");
                return false;
            }
        }

        /// <summary>
        /// Suorittaa sekvenssin aliprosessiin Black Liquor Fill kuuluvat vaiheet.
        /// </summary>
        /// <returns>totuusarvo: "true" jos Black Liquor Fill suoritettiin onnistuneesti loppuun asti, muuten "false"</returns>
        private bool BlackLiquorFill()
        {
            try
            {
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM3_OP2();
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM5_OP1();
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM4_OP1();

                bool move_on = false;
                while (move_on == false)
                {
                    lock (m_lockObject) { if (m_stopSequence)return false; }
                    try
                    {
                        int water_level = (int)changedItemsDict["LI400"].GetValue();
                        if (water_level < 35) { move_on = true; }
                    }
                    catch (KeyNotFoundException e) { }
                }

                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM3_OP6();
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM5_OP3();
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM4_OP2();

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Caught BlackLiquorFill()");
                return false;
            }
        }

        /// <summary>
        /// Suorittaa sekvenssin aliprosessiin White Liquor Fill kuuluvat vaiheet.
        /// </summary>
        /// <returns>totuusarvo: "true" jos White Liquor Fill suoritettiin onnistuneesti loppuun asti, muuten "false"</returns>
        private bool WhiteLiquorFill()
        {
            try
            {
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM3_OP3();
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM1_OP2();

                bool move_on = false;
                while (move_on == false)
                {
                    lock (m_lockObject) { if (m_stopSequence)return false; }
                    try
                    {
                        int T400_water_level = (int)changedItemsDict["LI400"].GetValue();
                        if (T400_water_level > 80) { move_on = true; }
                    }
                    catch (KeyNotFoundException e) { }
                }

                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM3_OP6();
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM1_OP4();

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Caught WhiteLiquorFill()");
                return false;
            }
        }

        /// <summary>
        /// Suorittaa sekvenssin aliprosessiin Cooking kuuluvat vaiheet.
        /// </summary>
        /// <returns>totuusarvo: "true" jos Cooking suoritettiin onnistuneesti loppuun asti, muuten "false"</returns>
        private bool Cooking()
        {
            try
            {
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM3_OP4();
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM1_OP1();

                bool cooking_temp_reached = false;

                while (cooking_temp_reached == false)
                {
                    if (T300Temperature >= cookingTemperature)
                    {
                        cooking_temp_reached = true;
                        opcClient.SetOnOffItem("E100", false);
                    }
                    //lock (m_lockObject) { if (m_stopSequence)return false; }
                }

                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM3_OP1();
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM1_OP2();

                DateTime start = DateTime.Now;
                while ((DateTime.Now - start).TotalMilliseconds <= cookingTime)
                {
                    lock (m_lockObject) { if (m_stopSequence)return false; }
                    U1_OP1();
                    U1_OP2();
                }

                lock (m_lockObject) { if (m_stopSequence)return false; }
                U1_OP3();
                lock (m_lockObject) { if (m_stopSequence)return false; }
                U1_OP4();
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM3_OP6();
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM1_OP4();
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM3_OP8();

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Caught Cooking()");
                return false;
            }
        }

        /// <summary>
        /// Suorittaa sekvenssin aliprosessiin Discharge kuuluvat vaiheet.
        /// </summary>
        /// <returns>totuusarvo: "true" jos Discharge suoritettiin onnistuneesti loppuun asti, muuten "false"</returns>
        private bool Discharge()
        {
            try
            {
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM5_OP2();
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM3_OP5();

                bool LS_minus300_activated = true;
                while (LS_minus300_activated)
                {
                    lock (m_lockObject) { if (m_stopSequence)return false; }
                    try
                    {
                        LS_minus300_activated = (bool)changedItemsDict["LS-300"].GetValue();
                    }
                    catch (KeyNotFoundException ex) { }
                }

                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM5_OP4();
                lock (m_lockObject) { if (m_stopSequence)return false; }
                EM3_OP7();
                return true;
            }
            catch
            {
                Console.WriteLine("Caught Discharge()");
                return false;
            }
        }

        /// <summary>
        /// Avaa venttiilit V102 ja V304, käynnistää pumpun P100 mikäli tankin T100 pinnankorkeus
        /// on vähintään turvatason verran, sekä käynnistää lämmittimen E100.
        /// </summary>
        private static void EM1_OP1()
        {
            opcClient.SetValveOpening("V102", 100); // Huom. V102 ja V104 ovat säätöventtiilejä
            opcClient.SetOnOffItem("V304", true);
            opcClient.SetOnOffItem("P100_P200_PRESET", true);

            bool above_safety_level = false;
            while (above_safety_level == false)
            {
                if (T100WaterLevel >= LI100_SAFETY_LEVEL)
                {
                    above_safety_level = true;
                    opcClient.SetPumpControl("P100", 100);
                }
            }
            opcClient.SetOnOffItem("E100", true);
        }

        /// <summary>
        /// Avaa venttiilit V102 ja V304, sekä käynnistää pumpun P100 mikäli tankin T100
        /// pinnankorkeus on vähintään turvatason verran.
        /// </summary>
        private static void EM1_OP2()
        {
            opcClient.SetValveOpening("V102", 100);
            opcClient.SetOnOffItem("V304", true);
            opcClient.SetOnOffItem("P100_P200_PRESET", true);

            bool above_safety_level = T100InSafetyLevel;
            while (above_safety_level == false)
            {
                try
                {
                    above_safety_level = (bool)changedItemsDict["LA+100"].GetValue();
                }
                catch (KeyNotFoundException ex) { }
            }
            opcClient.SetPumpControl("P100", 100);
        }

        /// <summary>
        /// Sulkee venttiilit V102 ja V304, sammuttaa pumpun P100 ja lämmittimen E100.
        /// </summary>
        private static void EM1_OP3()
        {
            opcClient.SetValveOpening("V102", 0);
            opcClient.SetOnOffItem("V304", false);
            opcClient.SetPumpControl("P100", 0);
            opcClient.SetOnOffItem("P100_P200_PRESET", false);
            opcClient.SetOnOffItem("E100", false);
        }

        /// <summary>
        /// Sulkee venttiilit V102 ja V304 ja sammuttaa pumpun P100.
        /// </summary>
        private static void EM1_OP4()
        {
            opcClient.SetValveOpening("V102", 0);
            opcClient.SetOnOffItem("V304", false);
            opcClient.SetPumpControl("P100", 0);
            opcClient.SetOnOffItem("P100_P200_PRESET", false);
        }

        /// <summary>
        /// Avaa venttiilin V201.
        /// </summary>
        private static void EM2_OP1()
        {
            opcClient.SetOnOffItem("V201", true);
        }

        /// <summary>
        /// Sulkee venttiilin V201.
        /// </summary>
        private static void EM2_OP2()
        {
            opcClient.SetOnOffItem("V201", false);
        }

        /// <summary>
        /// Sulkee venttiilit V104, V204 ja V401.
        /// </summary>
        private static void EM3_OP1()
        {
            opcClient.SetValveOpening("V104", 0);
            opcClient.SetOnOffItem("V204", false);
            opcClient.SetOnOffItem("V401", false);
        }

        /// <summary>
        /// Avaa venttiilit V204 ja V301.
        /// </summary>
        private static void EM3_OP2()
        {
            opcClient.SetOnOffItem("V204", true);
            opcClient.SetOnOffItem("V301", true);
        }

        /// <summary>
        /// Avaa venttiilit V301 ja V401.
        /// </summary>
        private static void EM3_OP3()
        {
            opcClient.SetOnOffItem("V301", true);
            opcClient.SetOnOffItem("V401", true);
        }

        /// <summary>
        /// Avaa venttiilit V104 ja V301.
        /// </summary>
        private static void EM3_OP4()
        {
            opcClient.SetValveOpening("V104", 100);
            opcClient.SetOnOffItem("V301", true);
        }

        /// <summary>
        /// Avaa venttiilit V204 ja V302.
        /// </summary>
        private static void EM3_OP5()
        {
            opcClient.SetOnOffItem("V204", true);
            opcClient.SetOnOffItem("V302", true);
        }

        /// <summary>
        /// Sulkee venttiilit V104, V204, V301, V401.
        /// </summary>
        private static void EM3_OP6()
        {
            opcClient.SetValveOpening("V104", 0);
            opcClient.SetOnOffItem("V204", false);
            opcClient.SetOnOffItem("V301", false);
            opcClient.SetOnOffItem("V401", false);
        }

        /// <summary>
        /// Sulkee venttiilit V302 ja V204.
        /// </summary>
        private static void EM3_OP7()
        {
            opcClient.SetOnOffItem("V302", true);
            opcClient.SetOnOffItem("V204", true);
        }

        /// <summary>
        /// Avaa venttiilin V204 ja sulkee sen, kun viive delayTime on kulunut.
        /// </summary>
        private static void EM3_OP8()
        {
            opcClient.SetOnOffItem("V204", true);
            System.Threading.Thread.Sleep(DELAY_TIME);
            opcClient.SetOnOffItem("V204", false);
        }

        /// <summary>
        /// Avaa venttiilin V4040.
        /// </summary>
        private static void EM4_OP1()
        {
            opcClient.SetOnOffItem("V404", true);
        }

        /// <summary>
        /// Sulkee venttiilin V404.
        /// </summary>
        private static void EM4_OP2()
        {
            opcClient.SetOnOffItem("V404", false);
        }

        /// <summary>
        /// Avaa venttiilin V303 ja käynnistää pumpun P200.
        /// </summary>
        private static void EM5_OP1()
        {
            opcClient.SetOnOffItem("V303", true);
            opcClient.SetOnOffItem("P100_P200_PRESET", true);
            opcClient.SetPumpControl("P200", 100);
        }

        /// <summary>
        /// Avaa venttiilit V103, V303 ja käynnistää pumpun P200.
        /// </summary>
        private static void EM5_OP2()
        {
            opcClient.SetOnOffItem("V103", true);
            opcClient.SetOnOffItem("V303", true);
            opcClient.SetOnOffItem("P100_P200_PRESET", true);
            opcClient.SetPumpControl("P200", 100);
        }

        /// <summary>
        /// Sulkee venttiilin V303 ja sammuttaa pumpun P200.
        /// </summary>
        private static void EM5_OP3()
        {
            opcClient.SetOnOffItem("V303", false);
            opcClient.SetPumpControl("P200", 0);
            opcClient.SetOnOffItem("P100_P200_PRESET", false);
        }

        /// <summary>
        /// Sulkee venttiilit V103, V303 ja sammuttaa pumpun P200.
        /// </summary>
        private static void EM5_OP4()
        {
            opcClient.SetOnOffItem("V103", false);
            opcClient.SetOnOffItem("V303", false);
            opcClient.SetPumpControl("P200", 0);
            opcClient.SetOnOffItem("P100_P200_PRESET", false);
        }

        /// <summary>
        /// Pitää tankin T300 paineen määritellyssä keittopaineessa kuristusventtiilillä V104.
        /// Keittovaihe keskeytetään, jos paine poikkeaa enemmän kuin 10 hPa asetusarvosta.
        /// </summary>
        private static void U1_OP1()
        {
            double pressureDiff = (double)(cookingPressure - T300Pressure);
            //if (Math.Abs(pressureDiff) > MAX_PRESSURE_DIFF)
            //{
             //   throw new Exception();
            //}
            if (pressureDiff >= PRESSURE_DIFF_TO_CONTROL)
            {
                opcClient.SetValveOpening("V104", 50);
            }
            else if (pressureDiff <= -PRESSURE_DIFF_TO_CONTROL)
            {
                opcClient.SetValveOpening("V104", 0);
            }
        }

        /// <summary>
        /// Pitää tankin T300 lämpötilan määritellyssä keittolämpötilassa lämmittimellä E100.
        /// Keittovaihe keskeytetään, jos lämpötila poikkeaa enemmän kuin 0.3 C asetusarvosta.
        /// </summary>
        private static void U1_OP2()
        {
            double tempDiff = cookingTemperature - T300Temperature;
            //if (Math.Abs(tempDiff) > MAX_TEMP_DIFF)
            //{
              //  throw new Exception();
            //}
            if (tempDiff >= TEMP_DIFF_TO_CONTROL)
            {
                opcClient.SetOnOffItem("E100", false);
            }
            else if (tempDiff <= -TEMP_DIFF_TO_CONTROL)
            {
                opcClient.SetOnOffItem("E100", true);
            }
        }

        /// <summary>
        /// Sulkee venttiilin V104.
        /// </summary>
        private static void U1_OP3()
        {
            opcClient.SetValveOpening("V104", 0);
        }

        /// <summary>
        /// Sammuttaa lämmittimen E100.
        /// </summary>
        private static void U1_OP4()
        {
            opcClient.SetOnOffItem("E100", false);
        }

        /// <summary>
        /// Lisää observerin säiliöön
        /// </summary>
        /// <param name="observer">lisättävä observer</param>
        public void AddObserver(IBackgroundTaskObserver observer)
        {
            lock (m_lockObject)
            {
                m_observers.Add(observer);
            }
        }

        /// <summary>
        /// Poistaa observerin säiliöstä
        /// </summary>
        /// <param name="observer">poistettava observer</param>
        public void RemoveObserver(IBackgroundTaskObserver observer)
        {
            lock (m_lockObject) 
            {
                m_observers.Remove(observer);
            }
        }

        /// <summary>
        /// Kutsuu observer objekteille funktiota muuttamaan GUI komponentteja säieturvallisesti
        /// </summary>
        /// <param name="T100">Säiliön T100 pinnankorkeus</param>
        /// <param name="T200">Säiliön T200 pinnankorkeus</param>
        /// <param name="T400">Säiliön T400 pinnankorkeus</param>
        /// <param name="temperature">Säiliön T300 lämpötila</param>
        /// <param name="pressure">Säiliön T300 paine</param>
        private void NotifyObserversOfValueChange()
        {
            IBackgroundTaskObserver[] observersCopy = null;

            int T100 = 0;
            int T200 = 0;
            int T400 = 0;
            double temperature = 0;
            int pressure = 0;

            lock (m_lockObject)
            {
                observersCopy = new IBackgroundTaskObserver[m_observers.Count];
                m_observers.CopyTo(observersCopy);

                T100 = T100WaterLevel;
                T200 = T200WaterLevel;
                T400 = T400WaterLevel;
                temperature = T300Temperature;
                pressure = T300Pressure;
            }

            foreach (IBackgroundTaskObserver o in observersCopy)
            {
                o.Update(T100, T200, T400, temperature, pressure);
            }
        }

        /// <summary>
        /// Kutsuu observer objekteille funktiota näyttämään ilmoituksen GUI:ssa säieturvallisesti
        /// </summary>
        /// <param name="message">näytettävä teksti</param>
        private void ObserverShowMessage(string message)
        {
            IBackgroundTaskObserver[] observersCopy = null;

            lock (m_lockObject)
            {
                observersCopy = new IBackgroundTaskObserver[m_observers.Count];
                m_observers.CopyTo(observersCopy);
            }

            foreach (IBackgroundTaskObserver o in observersCopy)
            {
                o.ShowMessage(message);
            }
        }

        /// <summary>
        /// Pysäyttää sekvenssin suorituksen ja resettaa laitteiston
        /// </summary>
        public void Dispose()
        {
            StopSequence();
            ResetEquipment();
        }
    }
}
