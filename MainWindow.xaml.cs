﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using minipanosprosessi;

namespace minipanosprosessi
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IBackgroundTaskObserver
    {
        private Program programInstance;

        private double T100max = 400;
        private double T200max = 400;
        private double T400max = 400;

        private double TempGoal = 100;
        private int PressureGoal = 100;

        /// <summary>
        /// Alustaa käyttöliittymän pääikkunan.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            programInstance = new Program();
            programInstance.AddObserver(this);
        }

        /// <summary>
        /// Kutsuu funktiota päivittämään GUI komponentteja UI säikeessä
        /// </summary>
        /// <param name="T100">Säiliön T100 pinnankorkeus</param>
        /// <param name="T200">Säiliön T200 pinnankorkeus</param>
        /// <param name="T400">Säiliön T400 pinnankorkeus</param>
        /// <param name="temperature">Säiliön T300 lämpötila</param>
        /// <param name="pressure">Säiliön T300 paine</param>
        public void Update(int T100, int T200, int T400, double temperature, int pressure)
        {
            var action = (Action)(() => UpdateProsessValues(T100, T200, T400, temperature, pressure));
            Dispatcher.BeginInvoke(action);
        }

        /// <summary>
        /// Kutsuu fukntiota päivittämään textboxin sisällön UI säikeessä
        /// </summary>
        /// <param name="message">päivitettävä teksti</param>
        public void ShowMessage(string message)
        {
            var action = (Action)(() => UpdateMessage(message));
            Dispatcher.BeginInvoke(action);
        }

        /// <summary>
        /// Kutsuu Program luokan instanssia aloittamaan sekvenssin suorituksen
        /// </summary>
        private void RunSequence()
        {
            programInstance.ExecuteSequence();
        }

        /// <summary>
        /// Kutsuu Program luokan instanssia lopettamaan sekvenssin suorituksen
        /// </summary>
        private void StopSequence()
        {
            programInstance.StopSequence();
        }
        /// <summary>
        /// Luo uuden säikeen missä kutsutaan funktiota aloittamaan sekvenssin suorittaminen
        /// </summary>
        /// <param name="sender">UI objekti, joka laukaisi tapahtuman</param>
        /// <param name="e">tapahtuma-argumentit</param>
        private void buttonStart_Click(object sender, RoutedEventArgs e)
        {
            var task = new System.Threading.Tasks.Task(() => RunSequence());
            task.Start();
        }

        /// <summary>
        /// Keskeyttää sekvenssin suorittamisen.
        /// </summary>
        /// <param name="sender">UI objekti, joka laukaisi tapahtuman</param>
        /// <param name="e">tapahtuma-argumentit</param>
        private void buttonInterrupt_Click(object sender, RoutedEventArgs e)
        {
            // pitääkö vaan keskeyttää suoritus vai myös reset tms.?
            var task = new System.Threading.Tasks.Task(() => StopSequence());
            task.Start();
        }

        /// <summary>
        /// Päivittää käyttäjän antamat parametrit mikäli ne ovat kelvolliset
        /// </summary>
        /// <param name="sender">UI objekti, joka laukaisi tapahtuman</param>
        /// <param name="e">tapahtuma-argumentit</param>
        private void buttonParameters_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int satTime = Int32.Parse(textBoxSaturationTime.Text);
                int cookTime = Int32.Parse(textBoxCookingTime.Text);
                double cookTemp = Double.Parse(textBoxCookingTemp.Text);
                int cookPressure = Int32.Parse(textBoxCookingP.Text);

                programInstance.ChangeInitValues(satTime, cookTime, cookTemp, cookPressure);
            }
            catch (Exception ex)
            {
                UpdateMessage("Tarkista syöttämäsi luvut");
            }
        }

        /// <summary>
        /// Päivittää GUI komponentteihin uudet arvot
        /// </summary>
        /// <param name="T100">Säiliön T100 pinnan korkeus</param>
        /// <param name="T200">Säiliön T200 pinnan korkeus</param>
        /// <param name="T400">Säiliön T400 pinnan korkeus</param>
        /// <param name="temperature">Säiliön T300 lämpötila</param>
        /// <param name="pressure">Säiliön T300 paine</param>
        private void UpdateProsessValues(int T100, int T200, int T400, double temperature, int pressure)
        {
            progressBarT100.Value = T100 / T100max * 100;
            progressBarT200.Value = T200 / T200max * 100;
            progressBarT400.Value = T400 / T400max * 100;

            double pressureBarValue = pressure / 350 * 100;

            if (temperature < 0)
            {
                temperature = 0;
            }
            else if (temperature > 100)
            {
                temperature = 100;
            }

            if (pressureBarValue < 0)
            {
                pressureBarValue = 0;
            }
            else if (pressureBarValue > 100)
            {
                pressureBarValue = 100;
            }

            progressBarTemperature.Value = temperature;
            progressBarPressure.Value = pressureBarValue;

            labelT100Value.Content = T100.ToString() + " mm";
            labelT200Value.Content = T200.ToString() + " mm";
            labelT400Value.Content = T400.ToString() + " mm";

            labelT0Value.Content = temperature.ToString("0.0") + " C";
            labelP0Value.Content = pressure.ToString("0.0") + " hPa";
        }

        /// <summary>
        /// Päivittää textBox laatikkoon tekstin
        /// </summary>
        /// <param name="message">päivitettävä teksti</param>
        private void UpdateMessage(string message)
        {
            textBox.Text = message;
        }

        /// <summary>
        /// Keskeyttää sekvenssin suorituksen kun ikkuna suljetaan
        /// </summary>
        /// <param name="sender">UI objekti, joka laukaisi tapahtuman</param>
        /// <param name="e">tapahtuma-argumentit</param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            programInstance.Dispose();
        }
    }
}
