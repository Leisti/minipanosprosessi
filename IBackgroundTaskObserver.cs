﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace minipanosprosessi
{
    interface IBackgroundTaskObserver
    {
        void Update(int T100, int T200, int T400, double temperature, int pressure);

        void ShowMessage(string message);
    }
}
